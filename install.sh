#!/bin/sh

# Add php5.6
sudo add-apt-repository ppa:ondrej/php
sleep 10

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 0DF731E45CE24F27EEEB1450EFDC8610341D9410
sleep 5

# Add SublimeText3
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sleep 5
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
sleep 5
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sleep 5
sudo apt-key fingerprint 0EBFCD88
sleep 5
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sleep 5
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sleep 5
sudo apt-get -f install
sleep 5

sudo apt-get update

sudo apt-get install wget apache2 mysql-server mysql-workbench mysql-utilities nano npm htop php7.2 php-xdebug php-xhprof \
 php7.2-cli php7.2-curl php7.2-enchant php7.2-gd php7.2-json php7.2-phpdbg php7.2-mbstring php7.2-mysql php7.2-zip php5.6 \
 php5.6-cli php5.6-curl php5.6-enchant php5.6-gd php5.6-json php5.6-mbstring php5.6-mysql php5.6-phpdbg php5.6-zip meld \
 git qgit filezilla composer multitail sublime-text  gnome-nettool gnome-network-admin  audacity keepass2 vlc vlc-plugin-samba \
 darktable alarm-clock-applet mc snapd shutter gimp inkscape uuid ntp docker

sleep 30

mkdir ~/apps
cd ~/apps

# GitKraken
wget https://release.gitkraken.com/linux/gitkraken-amd64.deb
sudo dpkg -i ~/apps/gitkraken-amd64.deb
rm ~/apps/gitkraken-amd64.deb

sleep 10

# AtomIO
wget https://atom-installer.github.com/v1.26.1/atom-amd64.deb
sudo dpkg -i ~/apps/atom-amd64.deb
rm ~/apps/atom-amd64.deb

sleep 10

# Toggl (https://support.toggl.com/toggl-desktop-for-linux/)
#wget https://toggl.com/api/v8/installer?app=td&platform=deb64&channel=stable
#wget http://fr.archive.ubuntu.com/ubuntu/pool/main/g/gst-plugins-base0.10/libgstreamer-plugins-base0.10-0_0.10.36-1_amd64.deb
#wget http://fr.archive.ubuntu.com/ubuntu/pool/universe/g/gstreamer0.10/libgstreamer0.10-0_0.10.36-1.5ubuntu1_amd64.deb
#sudo dpkg -i ~/apps/libgstreamer*.deb

sleep 10

# Slack
wget https://downloads.slack-edge.com/linux_releases/slack-desktop-3.1.1-amd64.deb
sudo dpkg -i ~/apps/slack-desktop-3.1.1-amd64.deb
rm ~/apps/slack-desktop-3.1.1-amd64.deb

sleep 10

# PhpStorm (https://www.jetbrains.com/help/phpstorm/installing-and-launching.html)
sudo snap install phpstorm --classic
sudo snap install pycharm-professional --classic
sudo snap install pycharm-community --classic
sudo snap install rubymine --classic

# Postman
wget https://dl.pstmn.io/download/version/6.0.10/linux64 | tar xzf ~/apps/postaman

sleep 10

# Google Chrome
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i ~/apps/google-chrome-stable_current_amd64.deb
rm ~/apps/google-chrome-stable_current_amd64.deb

sleep 10

# Dropbox
cd ~/apps wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xzf -
~/apps/.dropbox-dist/dropboxd
sudo apt-get install nautilus-dropbox

# Skype
sudo snap install skype --classic 


# Polarr
sudo snap install polarr

sleep 10

# Setup global gitconfig
touch ~/.gitconfig
echo "[user]
 	email = tahicz@gmail.com
 	name = Jan Tahovský
 [core]
 	autocrlf = input
 	safecrlf = false
 [alias]
 	f = fetch
 	# updates your branch with upstream (if fast-forward is possible)
 	ff = !git merge --ff-only `git rev-parse --symbolic-full-name --abbrev-ref=strict HEAD@{u}`
 	fp = fetch --prune
 	st = status
 	cm = commit
 	cma = commit --amend
 	br = branch
 	co = checkout
 	cp = cherry-pick
 	df = diff
 	rb = rebase
 	rbi = rebase -i
 	rbc = rebase --continue
 	rh = reset --hard
 	su = submodule update
 	# graph for current branch
 	l  = log --graph --decorate --pretty=oneline --abbrev-commit
 	# graph for all branches
 	ll = log --graph --decorate --pretty=oneline --abbrev-commit --all
 	# log for current branch showing diffs (-m is for showing mergecommits too)
 	ld = log -p -m
 	# log for current branch showing summary of changed files (-m is for showing mergecommits too)
 	ls = log --stat -m
 	# number of commits for each person
 	stats = shortlog -n -s --no-merges
 	# remove remote branch (remote must be named origin), usage: git rmb test
 	rmb = !sh -c 'git push origin :$1' -
 	# shows local > tracked remote
 	brt = for-each-ref --format=\\\"%(refname:short) > %(upstream:short)\\\" refs/heads
 	# get upstream tracked branch or error
 	brtracked = rev-parse --symbolic-full-name --abbrev-ref=strict HEAD@{u}
 	# commit all changes to a WIP commit
 	wip = !git add --all && git commit -m WIP
 [diff]
 	mnemonicprefix = true
 [pull]
 	rebase = true
 [push]
 	default = current
 [color]
 	ui = auto
 [color \"branch\"]
 	current = yellow reverse
 	local = yellow
 	remote = green
 [color \"diff\"]
 	meta = yellow bold
 	frag = magenta bold
 	old = red bold
 	new = green bold
 [color \"status\"]
 	added = green
 	changed = yellow
 	untracked = cyan
" > ~/.gitconfig

sleep 10

# SetUp default editor
echo "# Generated by /usr/bin/select-editor
SELECTED_EDITOR=\"/bin/nano\"
" > ~/.selected_editor
