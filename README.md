# New UBUNTU app instalation pack

Balík pro instalaci apliakcí do nové instalace Ubuntu + základní nastavení systému.

## Seznam aplikací:

- alarm-clock-applet
- apache2
- audacity
- atomIO
- composer
- darktable
- dropbox
- filezilla
- gimp
- git
- gitKraken
- gnome-nettool
- gnome-network-admin
- Google Chrome
- htop
- inkscape
- keepass2
- mc
- meld
- mysql-server
- mysql-workbench
- multitail
- nano
- npm
- qgit
- php5.6
- php5.6-cli
- php7.2
- php7.2-cli
- phpStorm
- php-xdebug
- php-xhprof
- Postaman
- polarr
- pyCharm (prof/comunity)
- rubyMine
- shutter
- skype
- slack
- snapd
- spotify
- sublime-text3
- uuid
- vlc

## Základní nastavení

- vytvoření globálního gitconfigu s aliasy
- nastavení defaultního editoru v terminálu

## Resetování ROOT hesla k MySQL serveru

Převzato z [StackOverflow](https://stackoverflow.com/questions/41645309/mysql-error-access-denied-for-user-rootlocalhost)

- Stop MySql server `sudo service mysql stop`
- Open & Edit `/etc/my.cnf`
- Add `skip-grant-tables` under [mysqld]
- Start MySql server `sudo service mysql start`
- You should be able to login to mysql now using the below command `mysql -u root -p`
- Run `mysql> flush privileges;`
- Set new password by `ALTER USER 'root'@'localhost' IDENTIFIED BY 'NewPassword';`
- Stop MySql server `sudo service mysql stop`
- Go back to `/etc/my.cnf` and remove/comment `skip-grant-tables`
- Start MySql server `sudo service mysql start`
    
Now you will be able to login with the new password `mysql -u root -p`

